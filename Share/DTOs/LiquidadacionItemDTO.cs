﻿using Share.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.DTOs
{
    public class LiquidadacionItemDTO
    {
        public long Id { get; set; }
        public TipoDocumento TipoDocumento { get; set; }
        public string Documento { get; set; }
        public string PrimerApellido { get; set; }
        public string PrimerNombre { get; set; }
        public decimal ImporteLiquidacion { get; set; }
    }
}

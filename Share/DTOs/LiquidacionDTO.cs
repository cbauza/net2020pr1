﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.DTOs
{
    public class LiquidacionDTO
    {
        public int CantidadEmpleados { get; set; }
        public decimal ImporteTotal { get; set; }
        public decimal ImporteTotalMensuales { get; set; }
        public decimal ImporteTotalJornaleros { get; set; }
        public int Anio { get; set; }
        public int Mes { get; set; }
        public List<LiquidadacionItemDTO> Items { get; set; }

        public LiquidacionDTO()
        {
            Items = new List<LiquidadacionItemDTO>();
        }
    }
}

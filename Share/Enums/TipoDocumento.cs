﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Share.Enums
{
    public enum TipoDocumento
    {
        CI = 1,
        OTRO = 2
    }
}

﻿using DataAccesLayer.Interfaces;
using Share.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesLayer.Implementations
{
    public class DAL_Empleado_Mock : IDAL_Empleado
    {
        public void DeleteDiaTrabaja(long IdDiaTrabaja)
        {
            throw new NotImplementedException();
        }

        public void DeleteEmpleado(long Id)
        {
            throw new NotImplementedException();
        }

        public Empleado GetEmpleado(long Id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> GetEmpleados()
        {
            throw new NotImplementedException();
        }

        public Empleado UpdateEmpleado(Empleado Empleado)
        {
            throw new NotImplementedException();
        }

        public EmpleadoDiaTrabaja AddDiaTrabaja(EmpleadoDiaTrabaja DiaTrabaja, long IdEmpleado)
        {
            throw new NotImplementedException();
        }

        public Empleado AddEmpleado(Empleado Empleado)
        {
            throw new NotImplementedException();
        }
    }
}

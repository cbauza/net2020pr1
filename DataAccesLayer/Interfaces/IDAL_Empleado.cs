﻿using Share.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccesLayer.Interfaces
{
    public interface IDAL_Empleado
    {
        List<Empleado> GetEmpleados();

        Empleado GetEmpleado(long Id);

        Empleado AddEmpleado(Empleado Empleado);

        Empleado UpdateEmpleado(Empleado Empleado);

        void DeleteEmpleado(long Id);

        EmpleadoDiaTrabaja AddDiaTrabaja(EmpleadoDiaTrabaja DiaTrabaja, long IdEmpleado);

        void DeleteDiaTrabaja(long IdDiaTrabaja);
    }
}

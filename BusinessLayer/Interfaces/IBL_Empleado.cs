﻿using Share.DTOs;
using Share.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IBL_Empleado
    {
        /// <summary>
        /// Obtiene la lista completa de todos los empleados.
        /// </summary>
        /// <returns></returns>
        List<Empleado> GetEmpleados();

        /// <summary>
        /// Obtiene la instancia de empleado cuyo id coincida con el id pasado.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Empleado GetEmpleado(long Id);

        /// <summary>
        /// Agrega una nueva instancia de empleado.
        /// Devuelve la nueva instancia recargada con el Id que le corresponde.
        /// </summary>
        /// <param name="Empleado"></param>
        /// <returns></returns>
        Empleado AddEmpleado(Empleado Empleado);

        /// <summary>
        /// Actualiza una instancia de empleado.
        /// Devuelve la nueva instancia recargada.
        /// </summary>
        /// <param name="Empleado"></param>
        /// <returns></returns>
        Empleado UpdateEmpleado(Empleado Empleado);

        /// <summary>
        /// Elimina definitivamente una instancia de empleado cuyo id coincida con el
        /// id pasado.
        /// </summary>
        /// <param name="Id"></param>
        void DeleteEmpleado(long Id);

        /// <summary>
        /// Agrega una instancia de EmpleadoDiaTrabaja para el empleado cuyo id se pasa,
        /// devuelve la instancia correspondiente con el id asociado.
        /// </summary>
        /// <param name="DiaTrabaja"></param>
        /// <returns></returns>
        EmpleadoDiaTrabaja AddDiaTrabaja(EmpleadoDiaTrabaja DiaTrabaja, long IdEmpleado);

        /// <summary>
        /// Elimina la instancia de EmpleadoDiaTrabaja cuyo id coincida con el parametro pasado.
        /// </summary>
        /// <param name="IdDiaTrabaja"></param>
        void DeleteDiaTrabaja(long IdDiaTrabaja);

        /// <summary>
        /// Calcula la liquidacion de la empresa para un mes y año en particular.
        /// Para los empleado jornaleros, calcula los días trabajados y las horas que trabajan
        /// por cada día, y eso se multiplica por el salario que equivale al salario / hora.
        /// Para el caso de los mensaules, el salario almacenado es el salario del mes, por lo tanto
        /// sencillamente el salario.
        /// </summary>
        /// <param name="Anio"></param>
        /// <param name="Mes"></param>
        /// <returns></returns>
        LiquidacionDTO GetLiquidacionMes(int Anio, int Mes);
    }
}

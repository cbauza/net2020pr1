﻿using BusinessLayer.Interfaces;
using DataAccesLayer.Interfaces;
using Share.DTOs;
using Share.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Implementations
{
    public class BL_Empleado : IBL_Empleado
    {
        private IDAL_Empleado dal;

        public BL_Empleado(IDAL_Empleado _dal)
        {
            dal = _dal;
        }

        public EmpleadoDiaTrabaja AddDiaTrabaja(EmpleadoDiaTrabaja DiaTrabaja, long IdEmpleado)
        {
            throw new NotImplementedException();
        }

        public Empleado AddEmpleado(Empleado Empleado)
        {
            throw new NotImplementedException();
        }

        public void DeleteDiaTrabaja(long IdDiaTrabaja)
        {
            throw new NotImplementedException();
        }

        public void DeleteEmpleado(long Id)
        {
            throw new NotImplementedException();
        }

        public Empleado GetEmpleado(long Id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> GetEmpleados()
        {
            throw new NotImplementedException();
        }

        public LiquidacionDTO GetLiquidacionMes(int Anio, int Mes)
        {
            throw new NotImplementedException();
        }

        public Empleado UpdateEmpleado(Empleado Empleado)
        {
            throw new NotImplementedException();
        }
    }
}
